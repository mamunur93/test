@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>{{ __('Login') }}</h3></div>

                <div class="card-body">
                   <!-- Custome Login Form -->
                    <form action="{{ route('login') }}" method="POST">
                      @csrf
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" placeholder="E-Mail Address" name="email" value="{{ old('email') }}">
                         @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="password">
                           @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="row align-items-center remember">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Login" class="btn float-right login_btn">
                    </div>
                </form>
               
                   <!-- end login -->
                </div>
                 <div class="card-footer">
                <div class="d-flex justify-content-center links">
                    <a href="{{ route('register') }}">Sign Up</a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="{{ route('password.request') }}">Forgot your password?</a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
