<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
 <!-- Toastr -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.js"></script>
<!-- Toastr -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Students Registation form</h2>
 

  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
  <!-- <form action="{{route('reg.store')}}" enctype="multipart/form-data" method="post"> -->

  

  	<form action="{{url('image/update/'.$data->id)}}" method="post" enctype="multipart/form-data">
        @csrf

    




    <div class="form-group">
      <label for="roll">Roll:</label>
      <select class="form-control" name="students_id" id="exampleFormControlSelect1">

     @foreach($student as $row)
      <option value="{{$row->id}}" @if ( $row->id == $data->students_id) {{ "Selected"}}
        @endif
       >{{$row->name}}</option>
      @endforeach


    </select>
    </div>

    <div class="form-group">
     	<label for="exampleFormControlFile1">Profile Picture</label>
   		<input type="file" name="images" class="form-control-file" id="exampleFormControlFile1">
   		<label for="exampleFormControlFile1">Current Image</label>
   		<img src="{{url($data->images)}}" style="width: 200px; height: 200px">

   		
   		<input type="hidden" name="old_images" value="{{$data->images}}">
    </div>

    
    
    <button type="submit" class="btn btn-default">Update</button>
  </form>
</div>



<script>
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>


</body>
</html>
