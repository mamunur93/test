@extends('pages.layout')

@section('main')
<div class="main">
            
            <p>
            London is the capital city of England. It is the most populous city in the United Kingdom, with a metropolitan area of over 13 million inhabitants.</p>
            <p>Standing on the River Thames, London has been a major settlement for two millennia, its history going back to its founding by the Romans, who named it Londinium.
            </p>
            <img src="{{asset('img/hello.png')}}">
        </div>
@endsection

@section('header')
<h1>Welcome to Hello World</h1>
@endsection