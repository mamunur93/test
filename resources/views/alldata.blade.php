@extends('layouts.app')
 

@section('content')
<div class="container">
  
  <h2>All Students</h2>
  <a class="btn btn-primary" href="{{url('yameen')}}" role="button">Download PDF</a>
  <a class="btn btn-primary" href="{{route('excel.export')}}" role="button">Download Excel</a>

              
  <table class="table" id="myTable">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Roll</th>
        <th>Create</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

     @foreach($data as $yameen)
      <tr>
        <td>{{$yameen->id}}</td>
        <td>{{$yameen->name}}</td>
        <td>{{$yameen->email}}</td>
        <td>{{$yameen->roll}}</td>
        <td>{{$yameen->created_at}}</td>
        <td>
          <a class="btn btn-primary" href="{{url('/view/'.$yameen->id)}}" role="button">View</a>
          <a class="btn btn-warning" href="{{url('/edit/'.$yameen->id)}}" role="button">Edit</a>
          <a class="btn btn-danger" href="{{url('/delete/'.$yameen->id)}}" role="button">Delete</a>
</td>
      </tr>
      
      @endforeach
    </tbody>
  </table>
</div>

@endsection

</body>
</html>
