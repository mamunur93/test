@extends('layouts.app')
 

@section('content')

<div class="container">
  <h2>All Students</h2>
              
  <table class="table" id="myTable">
    <thead>
      <tr>
        <th>Id</th>
        <th>students_id</th>
        <th>images</th>
        
        <th>Create</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

     @foreach($data as $yameen)
      <tr>
        <td>{{$yameen->id}}</td>
        <td>{{$yameen->name}}</td>
        <td><img src="{{url($yameen->images)}}" style="width: 300px; height: 200px"></td>
       
        <td>{{$yameen->created_at}}</td>
        <td>
          <a class="btn btn-primary" href="{{url('image/view/'.$yameen->id)}}" role="button">View</a>
          <a class="btn btn-warning" href="{{url('/image/edit/'.$yameen->id)}}" role="button">Edit</a>
          <a class="btn btn-danger" href="{{url('/image/delete/'.$yameen->id)}}" role="button">Delete</a>
</td>
      </tr>
      
      @endforeach
    </tbody>
  </table>
</div>@endsection