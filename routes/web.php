<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


/* Store data */

Route::get('/contact',function(){
	return view('pages.contact')->middleware('verified');
});
Route::post('/storessss','MeController@store')->name('store');
//Alldata view
Route::get('/alldata','MeController@all')->name('alldata')->middleware('verified');
//Single Data view
Route::get('/view/{id}','MeController@show');
Route::get('/delete/{id}','MeController@destroy');
Route::get('/edit/{id}','MeController@edit');
Route::post('/update/{id}','MeController@update');

//image upload
Route::get('/image/reg/', 'MeController@reg')->middleware('verified');
//Route::get('/registation', 'AllController@reg');
Route::post('reg/store','MeController@regstore')->name('reg.store');

Route::get('/image/all', 'MeController@ImageAll')->name('alluser')->middleware('verified');
Route::get('/image/view/{id}', 'MeController@ImgView');
Route::get('/image/delete/{id}', 'MeController@ImgDelete');
Route::get('/image/edit/{id}', 'MeController@ImgEdit');
Route::post('/image/update/{id}', 'MeController@ImgUpdate');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('/yameen','MeController@yameen');
Route::get('/export', 'MeController@excel')->name('excel.export');
