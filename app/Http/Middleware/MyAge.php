<?php

namespace App\Http\Middleware;

use Closure;

class MyAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->name > 200) {
            return redirect('about');
        }
        return $next($request);
    }
}
