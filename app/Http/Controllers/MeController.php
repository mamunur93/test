<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use DB;
use PDF;

class MeController extends Controller
{
    

     public function all()
    {
       $data=DB::table('students')->get();

       return view('alldata', compact('data'));
    }

    public function store(Request $yameen){

         $validatedData = $yameen->validate([
        'name' => 'required|max:255|min:5',
        'email' => 'required|unique:students',
        'roll' => 'required|min:3|unique:students',
    ]);

        $data=array();
        $data['name']=$yameen->name;
        $data['email']=$yameen->email;
        $data['roll']=$yameen->roll;

        $student=DB::table('students')->insert($data);
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/
        if ($student) {
            
            $notification = array(
                'message' => 'Data Insert Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('alldata')->with($notification);
        }
        else{
             $notification = array(
                'message' => 'Sorry ! Data Insert Failed', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
        }
    }

     public function show($id)
    {
        // echo $id;
        // die();
        $data=DB::table('students')->where('id',$id)->first();
        return view('view', compact('data'));
    }

    public function destroy($id){
         $data=DB::table('students')->where('id',$id)->delete();
         if ($data) {
            
            $notification = array(
                'message' => 'Data delete Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('alldata')->with($notification);
        }
        else{
             $notification = array(
                'message' => 'Sorry ! Data delete Failed', 
                'alert-type' => 'error'
                );

            return Redirect()->route('alldata')->with($notification);
        }
        
    }

    public function edit($id){

       $data=DB::table('students')->where('id', $id)->first();
       return view('edit', compact('data'));

    }

    public function update(Request $req, $id){

         $validatedData = $req->validate([
        'name' => 'required|max:255|min:5',
        'email' => 'required',
        'roll' => 'required|min:3',
    ]);

        $data=array();
        $data['name']=$req->name;
        $data['email']=$req->email;
        $data['roll']=$req->roll;

        $update=DB::table('students')->where('id',$id)->update($data);
        
        if ($update) {
            
            $notification = array(
                'message' => 'Data update Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('alldata')->with($notification);
        }
        else{
             $notification = array(
                'message' => 'Nothing to update', 
                'alert-type' => 'error'
                );

            return Redirect()->route('alldata')->with($notification);
        }
    }

    public function reg(){
        $data=DB::table('students')->get();

       return view('registation', compact('data'));
    }
    public function regstore(Request $request){

        $validatedData = $request->validate([
        'students_id' => 'required',
        'images' => 'required | mimes:jpeg,jpg,png | max:1000',
    ]);
        $data=array();
        $data['students_id']= $request->students_id;
        $image=$request->file('images');
       
       
        $image_name=hexdec(uniqid());
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='public/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            $data['images']=$image_url;



        $registation=DB::table('registation')->insert($data);
        
        if ($registation) {
            
            $notification = array(
                'message' => 'Registation Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('alluser')->with($notification);
        }
        else{
             $notification = array(
                'message' => 'Sorry ! Registation Failed', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
        }

    }

    public function ImageAll(){
        //$data=DB::table('registation')->get();
        $data=DB::table('registation')->join('students','registation.students_id','students.id')->select('registation.*', 'students.name' )->get();

        return
         view('alluser', compact('data') );
    }

    public function ImgView($id){

         $data=DB::table('registation')->join('students','registation.students_id','students.id')->select('registation.*', 'students.name' )->where('registation.id',$id)->first();
         return
         view('singleimage', compact('data') );
    }

    public function ImgDelete($id){
            $single=DB::table('registation')->where('id',$id)->first();
            $images=$single->images;

        $delete=DB::table('registation')->where('id',$id)->delete();
         if ($delete) {
            unlink($images);
            $notification = array(
                'message' => 'Data delete Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('alluser')->with($notification);
    }
    else{
        $notification = array(
         'message' => 'Data delete failed!', 
                'alert-type' => 'error'
                );

            return Redirect()-> route('alluser')->with($notification);
            }
    }

    public function ImgEdit($id){
        $student=DB::table('students')->get();

        $data=DB::table('registation')->where('id', $id)->first();
        return view ('imageedit', compact('student','data'));

    }

    public function ImgUpdate(Request $request, $id){
         $validatedData = $request->validate([
        'students_id' => 'required',
        'images' => ' mimes:jpeg,jpg,png | max:1000',
    ]);

        $data=array();
        $data['students_id']= $request->students_id;
        $image=$request->file('images');
       
       
        
        
        if ($image) {
                $image_name=hexdec(uniqid());
            $ext=strtolower($image->getClientOriginalExtension());
            $image_full_name=$image_name.'.'.$ext;
            $upload_path='public/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            $data['images']=$image_url;
            $old_image=$request->old_images;
            unlink($old_image);

        $registation=DB::table('registation')->where('id', $id)->update($data);



            $notification = array(
                'message' => 'update Successful!', 
                'alert-type' => 'success'
                );

            return Redirect()-> route('alluser')->with($notification);
        }
        else{
            $data['images']= $request->old_images;

            $registation=DB::table('registation')->where('id', $id)->update($data);

             $notification = array(
                'message' => 'no changes', 
                'alert-type' => 'error'
                );

            return Redirect()-> back()->with($notification);
        }


    }

    public function yameen(){
        $data=DB::table('students')->get();
        $pdf = PDF::loadView('alluserpdf', compact('data'));
  
        return $pdf->download('alluserpdf.pdf');

    }
    public function excel(){
        
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}